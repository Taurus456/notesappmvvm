package ru.ako.notesappmvvm

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ru.ako.notesappmvvm.database.room.AppRoomDatabase
import ru.ako.notesappmvvm.database.room.repository.RoomRepository
import ru.ako.notesappmvvm.model.Note
import ru.ako.notesappmvvm.utils.REPOSITORY
import ru.ako.notesappmvvm.utils.TYPE_FIREBASE
import ru.ako.notesappmvvm.utils.TYPE_ROOM
import java.lang.IllegalArgumentException

class MainVeiwModel(application: Application) : AndroidViewModel(application) {

    val context = application

    fun initDatebase(type: String, onSuccess: () -> Unit) {
        Log.d("checkData", "MainVeiwModel initDatebase with type: $type")
        when (type) {
            TYPE_ROOM -> {
                val dao = AppRoomDatabase.getInstance(context = context).getRoomDao()
                REPOSITORY = RoomRepository(dao)
                onSuccess()
            }
        }
    }

    fun addNote(note: Note, onSuccess: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            REPOSITORY.create(note = note) {
                viewModelScope.launch(Dispatchers.Main) {
                    onSuccess()
                }
            }
        }
    }

    fun updateNote(note: Note, onSuccess: () -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            REPOSITORY.update(note = note) {
                viewModelScope.launch(Dispatchers.Main) {
                    onSuccess()
                }
            }
        }
    }

    fun deleteNote(note: Note, onSuccess: () -> Unit){
        viewModelScope.launch(Dispatchers.IO) {
            REPOSITORY.delete(note = note) {
                viewModelScope.launch(Dispatchers.Main){
                    onSuccess()
                }
            }
        }
    }

    fun readAllNotes() = REPOSITORY.readAll
}

class MainVeiwModelFactory(private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainVeiwModel::class.java))
            return MainVeiwModel(application = application) as T
        throw IllegalArgumentException("Unknown ViewModel")
    }

}