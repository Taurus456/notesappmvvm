package ru.ako.notesappmvvm.screens


import android.app.Application
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import ru.ako.notesappmvvm.MainVeiwModel
import ru.ako.notesappmvvm.MainVeiwModelFactory
import ru.ako.notesappmvvm.navigation.NavRoute
import ru.ako.notesappmvvm.ui.theme.NotesAppMVVMTheme
import ru.ako.notesappmvvm.utils.Constants
import ru.ako.notesappmvvm.utils.TYPE_FIREBASE
import ru.ako.notesappmvvm.utils.TYPE_ROOM

@Composable
fun StartScreen(navController: NavHostController, viewModel: MainVeiwModel) {
    val context = LocalContext.current
    val mViewModel: MainVeiwModel =
        viewModel(factory = MainVeiwModelFactory(context.applicationContext as Application))
    Scaffold(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(text = Constants.Keys.WHAT_WILL_WE_USE)
            Button(
                onClick = {
                    mViewModel.initDatebase(TYPE_ROOM) {
                        navController.navigate(route = NavRoute.Main.route)
                    }
                },
                modifier = Modifier
                    .width(200.dp)
                    .padding(vertical = 8.dp)
            ) {
                Text(text = Constants.Keys.ROOM_DATABASE)
            }
            Button(
                onClick = {
                    mViewModel.initDatebase(TYPE_FIREBASE) {
                        navController.navigate(route = NavRoute.Main.route)
                    }
                },
                modifier = Modifier
                    .width(200.dp)
                    .padding(vertical = 8.dp)
            ) {
                Text(text = Constants.Keys.FIREBASE_DATABASE)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun prevStartScreen() {
    NotesAppMVVMTheme {
        val context = LocalContext.current
        val mViewModel: MainVeiwModel =
            viewModel(factory = MainVeiwModelFactory(context.applicationContext as Application))
        StartScreen(navController = rememberNavController(), viewModel = mViewModel)

    }
}